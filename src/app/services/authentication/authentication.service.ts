import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from 'environments/environment.dev';

@Injectable()
export class AuthenticationService {

  constructor(private http: HttpClient) { }

  /*** methods/actions ***/
  login(username: string, password: string) {
    return this.http.post<any>(environment.AUTH + `/Auth`, {username: username, password: password})
      .pipe(map(user => {
        if(user && user.result.accessToken) {
          // store user details and jwt to localstorage
          localStorage.setItem('currentUser', JSON.stringify(user.result))
        }
        return user;
      }));
  }

  logout() {
    // remove user from local storage
    localStorage.removeItem('');
  }

  identity() {
    return this.http.get<any>(environment.AUTH + `/Identity`)
      .pipe(map(result => {
        return result;
      }));
  }

}
