import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from 'environments/environment.dev';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http: HttpClient) { }

  /** methods/actions **/
  register(company: string, username: string, email: string, phone: string) {
    return this.http.post<any>(environment.AUTH + `/Register`, {company: company, username: username, email: email, phone: phone})
      .pipe(map(result => {
        return result;
      }));
  }

}
